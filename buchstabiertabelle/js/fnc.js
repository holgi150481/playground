document.addEventListener("DOMContentLoaded", setSprache, false);

function auswertung() {
	textfeld = document.getElementById('eingabe').value;
	
	tbl=document.getElementById('tabelle');
	while(tbl.rows.length > 0) {
    	tbl.deleteRow(0);
  	}	

	for (i=0; i < textfeld.length; i++) {
		ausgabe = umwandeln(textfeld[i].toLowerCase(), sprache);	
		formatiert = ausgabe.slice(0, 1).fontsize(6) + ausgabe.slice(1).fontsize(4);
		document.getElementById('tabelle').insertRow(-1).insertCell(0).innerHTML = formatiert;
	};
}

function setSprache() {
	if (document.getElementsByName('sprache')[0].checked == true) {
  		sprache = DE;
  	} else {
  		sprache = NATO;
  	}
  	auswertung();
}

function leeren() {
	document.getElementById('eingabe').value = "";
	
	tbl=document.getElementById('tabelle');
	while(tbl.rows.length > 0) {
    	tbl.deleteRow(0);
  	}	
}

