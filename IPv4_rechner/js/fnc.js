function dec2bin(value) {
	var result = parseInt(value);
	result = result.toString(2);	

	for (var i = result.length; i < 8; i++) {
		result = '0' + result;
	}
	return result;
}
 
function bin2dec(value) {
	var result = parseInt(value,2);
	return result;
}

function calcIp(oktett, item) {	
	if (oktett.value < 0 || oktett.value > 255) {
		alert("Fehlerhafte Eingabe\nNur 0 - 255 erlaubt!");
		document.getElementById('ip' + item).value = 0;		
	} else {
		var result = dec2bin(oktett.value);
		document.getElementById('oktett' + item).innerHTML = result;
	}
	getInfo();
}

function validateSubnet(item, oktett) {	
	for (var i = 1; i < oktett; i++) {	
		document.getElementById('subnet' + i).value = 255;
		document.getElementById('subOktett' + i).innerHTML = dec2bin(255);
	}		
	for (var j = oktett+1; j <= 4; j++) {	
		document.getElementById('subnet' + j).value = 0;
		document.getElementById('subOktett' + j).innerHTML = dec2bin(0);		
	}		
	document.getElementById('subOktett' + oktett).innerHTML = dec2bin(item.value);
	
	getInfo();
}

function getInfo() {	
	// Subnet Binär auslesen
	var subnet = document.getElementById('subOktett1').innerHTML;
	subnet = subnet.concat(document.getElementById('subOktett2').innerHTML);
	subnet = subnet.concat(document.getElementById('subOktett3').innerHTML);
	subnet = subnet.concat(document.getElementById('subOktett4').innerHTML);

	// IP Binär auslesen
	var ip = document.getElementById('oktett1').innerHTML;
	ip += document.getElementById('oktett2').innerHTML;
	ip += document.getElementById('oktett3').innerHTML;
	ip += document.getElementById('oktett4').innerHTML;
	
	// NetID und Broadcast berechnen	
	var netId = "";
	for (var i = 0; i < 32; i++) {
		netId += subnet[i] & ip[i]; 
	}

	var invSubnet = "";
	for (var i = 0; i < 32; i++) {
		if (subnet[i] == 1) {
			invSubnet += 0;	
		} else {
			invSubnet += 1;
		}
	}

	var broadcast = "";
	for (var i = 0; i < 32; i++) {
		broadcast += ip[i] | invSubnet[i]; 
	}

	document.getElementById('netId').innerHTML = bin2dec(netId.slice(0,8)) + '.' + bin2dec(netId.slice(8,16)) + '.' + bin2dec(netId.slice(16,24)) + '.' + bin2dec(netId.slice(24,32));
	document.getElementById('broadcast').innerHTML = bin2dec(broadcast.slice(0,8)) + '.' + bin2dec(broadcast.slice(8,16)) + '.' + bin2dec(broadcast.slice(16,24)) + '.' + bin2dec(broadcast.slice(24,32));

	// Start und EndIp berechnen
	var start = (bin2dec(netId.slice(24.32)) + 1);
	var end = (bin2dec(broadcast.slice(24.32)) - 1);
	document.getElementById('startIp').innerHTML = bin2dec(netId.slice(0,8)) + '.' + bin2dec(netId.slice(8,16)) + '.' + bin2dec(netId.slice(16,24)) + '.' + start;
	document.getElementById('endIp').innerHTML = bin2dec(broadcast.slice(0,8)) + '.' + bin2dec(broadcast.slice(8,16)) + '.' + bin2dec(broadcast.slice(16,24)) + '.' + end;

	// Notation und Hosts berechnen
	subnetSplit = subnet.split(0);
	var notation = subnetSplit['0'].length;
	var hosts = Math.pow(2, 32 - notation) - 2;
	document.getElementById('hosts').innerHTML = hosts;
	document.getElementById('notation').innerHTML = '/' + notation;
}