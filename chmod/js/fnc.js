var chmod = 0;

function calc(val,opt) {
		
	if (opt.checked) {
		chmod = chmod + val;
	} else {
		chmod = chmod - val;
	}
	
	ausgabe = chmod.toString();
	
	if (ausgabe.length == 2) {
		ausgabe = "0" + ausgabe;
	} else if (ausgabe.length == 1) {
		ausgabe = "00" + ausgabe;
	} 
	
	digit(1,ausgabe.slice(0,1));
	digit(2,ausgabe.slice(1,2));
	digit(3,ausgabe.slice(2,3));
	
}

function digit(num, src) {
	document.getElementById(num).src = "img/" + src + ".gif";
}