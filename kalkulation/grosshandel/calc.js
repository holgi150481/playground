$(document).ready(function() {
	
	$('#btn_reset').click(function() {
		$('.field input').each(function() {
			$(this).val("");
		})
	});
	
	$('#btn_print').click(function() {
		print();
	});
	
	$('.field input').change(function() {
		
		$('#out_listeneinkaufspreis').val(round(parseFloat($('#in_listeneinkaufspreis').val())));
		$('#out_listeneinkaufspreis_links').val(100);

		$('#out_liefererrabatt').val(round(parseFloat($('#in_listeneinkaufspreis').val()) * parseFloat($('#in_lieferrabatt').val()) / 100));
		$('#out_liefererrabatt_links').val($('#in_lieferrabatt').val());
		
		$('#out_zieleinkaufspreis').val(round(parseFloat($('#out_listeneinkaufspreis').val()) - parseFloat($('#out_liefererrabatt').val())));
		$('#out_zieleinkaufspreis_links').val(toInt($('#out_listeneinkaufspreis_links').val()) - toInt($('#in_lieferrabatt').val()));
		$('#out_zieleinkaufspreis_rechts').val(100);
		
		$('#out_liefererskonto').val(round(parseFloat($('#out_zieleinkaufspreis').val()) * parseFloat($('#in_liefererskonto').val()) / 100));
		$('#out_liefererskonto_rechts').val($('#in_liefererskonto').val());
		
		$('#out_bareinkaufspreis').val(round(parseFloat($('#out_zieleinkaufspreis').val()) - parseFloat($('#out_liefererskonto').val())));
		$('#out_bareinkaufspreis_rechts').val(toInt($('#out_zieleinkaufspreis_rechts').val()) - toInt($('#in_liefererskonto').val()));
		
		$('#out_bezugskosten').val(round((parseFloat($('#in_bezugskosten').val()))));
		
		$('#out_bezugspreis').val(round(parseFloat($('#out_bareinkaufspreis').val()) + parseFloat($('#out_bezugskosten').val())));
		$('#out_bezugspreis_links').val(100);
		
		$('#out_handlungskosten').val(round(parseFloat($('#out_bezugspreis').val()) * parseFloat($('#in_handlungskosten').val()) / 100));
		$('#out_handlungskosten_links').val($('#in_handlungskosten').val());
		
		$('#out_selbstkostenpreis').val(round(parseFloat($('#out_handlungskosten').val()) + parseFloat($('#out_bezugspreis').val())));
		$('#out_selbstkostenpreis_links').val(toInt($('#out_bezugspreis_links').val()) + toInt($('#out_handlungskosten_links').val()));
		$('#out_selbstkostenpreis_rechts').val(100);
		
		$('#out_gewinn').val(round(parseFloat($('#out_selbstkostenpreis').val()) * parseFloat($('#in_gewinn').val()) / 100));
		$('#out_gewinn_rechts').val($('#in_gewinn').val());
		$('#out_selbstkostenpreis_links').val(toInt($('#out_bezugspreis_links').val()) + toInt($('#out_handlungskosten_links').val()));
		
		$('#out_barverkaufspreis').val(round(parseFloat($('#out_gewinn').val()) + parseFloat($('#out_selbstkostenpreis').val())));
		$('#out_barverkaufspreis_rechts').val(toInt($('#out_gewinn_rechts').val()) + toInt($('#out_selbstkostenpreis_rechts').val()));
		$('#out_barverkaufspreis_links').val(100 - toInt($('#in_kundenskonto').val()));
		
		$('#out_kundenskonto').val(round(parseFloat($('#out_barverkaufspreis').val()) / parseFloat($('#out_barverkaufspreis_links').val()) * parseFloat($('#in_kundenskonto').val())));
		$('#out_kundenskonto_links').val($('#in_kundenskonto').val());
		
		$('#out_zielverkaufspreis').val(round(parseFloat($('#out_kundenskonto').val()) + parseFloat($('#out_barverkaufspreis').val())));
		$('#out_zielverkaufspreis_links').val(100);
		$('#out_zielverkaufspreis_rechts').val(100 - toInt($('#in_kundenrabatt').val()));
		
		$('#out_kundenrabatt_rechts').val($('#in_kundenrabatt').val());
		$('#out_kundenrabatt').val(round(parseFloat($('#out_zielverkaufspreis').val()) / parseFloat($('#out_zielverkaufspreis_rechts').val()) * parseFloat($('#in_kundenrabatt').val())));
		
		$('#out_nettoverkaufspreis').val(round(parseFloat($('#out_kundenrabatt').val()) + parseFloat($('#out_zielverkaufspreis').val())));
		$('#out_nettoverkaufspreis_rechts').val(100);
		
	});
	
});

function toInt(value) {
	var result = parseInt(value);
	if (isNaN(result)) {
		return value;
	} else {
		return result
	}
}

function round(value) {
	var result = Math.round(value * 100) / 100;
	if (isNaN(result)) {
		return '0.00';
	} else {
		return result.toFixed(2);
	}	
}