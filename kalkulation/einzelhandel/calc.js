$(document).ready(function() {
	
	$('#btn_reset').click(function() {
		$('.field input').each(function() {
			$(this).val("");
		})
	});
	
	$('#btn_print').click(function() {
		print();
	});
	
	$('.field input').change(function() {
		$('#out_listeneinkaufspreis').val(round(parseFloat($('#in_listeneinkaufspreis').val())));
		$('#out_rabatt').val(round(parseFloat($('#in_listeneinkaufspreis').val()) * parseFloat($('#in_rabatt').val()) / 100));
		$('#out_zieleinkaufspreis').val(round(parseFloat($('#out_listeneinkaufspreis').val()) - parseFloat($('#out_rabatt').val())));
		$('#out_skonto').val(round(parseFloat($('#out_zieleinkaufspreis').val()) * parseFloat($('#in_skonto').val()) / 100));
		$('#out_bareinkaufspreis').val(round(parseFloat($('#out_zieleinkaufspreis').val()) - parseFloat($('#out_skonto').val())));
		$('#out_bezugskosten').val(round((parseFloat($('#in_bezugskosten').val()))));
		$('#out_bezugspreis').val(round(parseFloat($('#out_bareinkaufspreis').val()) + parseFloat($('#out_bezugskosten').val())));
		$('#out_handlungskostenzuschlag').val(round(parseFloat($('#out_bezugspreis').val()) + parseFloat($('#in_handlungskostenzuschlag').val()) / 100));
		$('#out_selbstkostenpreis').val(round(parseFloat($('#out_handlungskostenzuschlag').val()) + parseFloat($('#out_bezugspreis').val())));
		$('#out_gewinnzuschlag').val(round(parseFloat($('#out_selbstkostenpreis').val()) * parseFloat($('#in_gewinnzuschlag').val()) / 100));
		$('#out_nettoverkaufspreis').val(round(parseFloat($('#out_gewinnzuschlag').val()) + parseFloat($('#out_selbstkostenpreis').val())));
		$('#out_umsatzsteuer').val(round(parseFloat($('#out_nettoverkaufspreis').val()) * parseFloat($('#in_umsatzsteuer').val()) / 100));
		$('#out_bruttoverkaufspreis').val(round(parseFloat($('#out_umsatzsteuer').val()) + parseFloat($('#out_nettoverkaufspreis').val())));
	});
	
});

function round(value) {
	var result = Math.round(value * 100) / 100;
	if (isNaN(result)) {
		return '0.00';
	} else {
		return result.toFixed(2);
	}	
}