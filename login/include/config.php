<?php
session_start();

// MySQL Verbindung
define('DB_HOST', 'localhost');
define('DB_NAME', 'sqltest');
define('DB_USER', 'phpuser');
define('DB_PASS', 'secret');

$db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

// Tabellen
define('TABLE_LOGIN', 'auth_login');
?>