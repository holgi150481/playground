<?php
require('include/config.php');
define('REQUIRED_LEVEL', '0');
require('include/auth.php');

if (isset($_REQUEST['login'])) {
	$sql = 'SELECT user_level, name, password FROM ' . TABLE_LOGIN . ' WHERE name=' . $_REQUEST['name'] . ' AND password=' . $_REQUEST['password'];	
	$query = $db->query($sql);
	if ($query->num_rows != 0) {
		$user_data = $query->fetch_assoc();		
		$_SESSION['user_level'] = $user_data['user_level'];
		header('Location: ' . $_SERVER['PHP_SELF ']);
	} 
}

if (isset($_REQUEST['logout'])) {
	unset($_SESSION);
	session_destroy();
	header('Location: ' . $_SERVER['PHP_SELF ']);
	exit;	
}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1" />
		<title>Login</title>
	</head>
	<body>
		<h1>..:: Login ::...</h1>
		<a href="admin.php">Admin</a>
		<a href="register.php">Registrieren</a>
		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
			<label for="use">Benutzername:</label>
			<input type="text" name="name"/>
			<label for="pass">Passwort</label>
			<input type="text" name="password" />
			<input type="submit" name="login" value="Login" />
			<input type="submit" name="logout" value="Logout" />
		</form>
	</body>
</html>