<?php
define('REQUIRED_LEVEL', '10');
require('include/config.php');
require('include/auth.php');

if (isset($_REQUEST['edit'])) {
	$sql = 'SELECT * FROM ' . TABLE_LOGIN . ' WHERE user_id = ' . $_REQUEST['edit'];
	$query = $db->query($sql);
	$entry = $query->fetch_assoc();	
	$edited = 'yes';
}

if (isset($_REQUEST['del'])) {
	$sql = 'DELETE FROM ' . TABLE_LOGIN . ' WHERE user_id = ' . $_REQUEST['del'];
	$db->query($sql); 
}

if (isset($_REQUEST['submit'])) {
	if (empty($_REQUEST['user_id'])) {
		$sql = 'INSERT INTO ' . TABLE_LOGIN . ' (reg_date, user_level, name, password) VALUES (NOW(), '
		 . $_REQUEST['user_level'] . ', '
		 . $_REQUEST['name'] . ', '
		 . $_REQUEST['password'] . ')';	
	} else {
		$sql = 'UPDATE ' . TABLE_LOGIN . ' SET 
		name=' . $_REQUEST['name'] . ', 
		password=' . $_REQUEST['password'] . ',
		user_level=' . $_REQUEST['user_level'] . 
		' WHERE user_id=' . $_REQUEST['user_id'];
		
	}
	$db->query($sql);
}

$sql = 'SELECT *, DATE_FORMAT(reg_date, "%d.%c.%Y") AS reg_date FROM ' . TABLE_LOGIN;
$query = $db->query($sql);

while ($row = $query->fetch_assoc()) {
	$user_data[] = $row;
}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1" />
		<title>Login - Administration</title>
	</head>
	<body>
		<h1>..:: Login - Administration ::...</h1>
		<a href="index.php">Zur�ck</a>
		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
			<label for="use">Benutzername:</label>
			<input type="text" name="name"/ value="<?php echo $entry['name']; ?>" required>
			<label for="pass">Passwort</label>
			<input type="text" name="password" value="<?php echo $entry['password']; ?>"/ required>
			<label for="use">Level:</label>
			<input type="text" name="user_level"value="<?php echo $entry['user_level']; ?>"/ required>
			<input type="hidden" name="user_id" value="<?php echo $entry['user_id']; ?>" />			
			<input type="submit" name="submit" value="Speichern" />			
		</form>
		<?php if(!empty($user_data)) { ?>
					<table border="1">			
				<tr>
					<th>Reg. Datum</th>
					<th>Name</th>
					<th>Passwort</th>
					<th>Level</th>
					<th>Aktion</th>
				</tr>
				<?php foreach ($user_data as $key => $value) { ?>
					<tr>
						<td><?php echo $value['reg_date']; ?></td>
						<td><?php echo $value['name']; ?></td>
						<td><?php echo $value['password']; ?></td>
						<td><?php echo $value['user_level']; ?></td>
						<td>
							<a href="<?php echo $_SERVER['PHP_SELF'] ?>?edit=<?php echo $value['user_id'] ?>">Bearbeiten</a>
							<a href="<?php echo $_SERVER['PHP_SELF'] ?>?del=<?php echo $value['user_id'] ?>">L�schen</a>
						</td>
					</tr>
				<?php } ?>
			</table>
		<?php } else { ?>
			<h2>Keine Daten Vorhanden</h2>
		<?php } ?>
	</body>
</html>