<?php
function getFolders() {
    $total = 0;
    $free = 0;
    $used = 0;

    $mounts = shell_exec('ls /media');
    $mounts = preg_split('/[\r\n]+/', $mounts);

    foreach($mounts as $mount) {
        if (strpos($mount, 'cdrom') !== false) continue;
        $folder = '/media/' . $mount;
        $data[] = Array(
            'folder' => $folder,
            'total' => disk_total_space($folder),
            'free' => disk_free_space($folder),
            'used' => disk_total_space($folder) - disk_free_space($folder),
            'percent' =>  round((disk_total_space($folder) - disk_free_space($folder)) / disk_total_space($folder) * 100, 2)
        );
        $total += disk_total_space($folder);
        $free += disk_free_space($folder);
        $used += (disk_total_space($folder) - disk_free_space($folder));
    }

    $data['total'] = Array(
        'total' => $total,
        'free' => $free,
        'used' => $used,
        'percent' => round($used / $total * 100,2)
    );

    return $data;
}

function format_bytes($a_bytes) {
    if ($a_bytes < 1024) {
        return $a_bytes .' Byte';
    } elseif ($a_bytes < pow(1024, 2)) {
        return round($a_bytes / 1024, 2) .' KB';
    } elseif ($a_bytes < pow(1024, 3)) {
        return round($a_bytes / pow(1024, 2), 2) . ' MB';
    } elseif ($a_bytes < pow(1024, 4)) {
        return round($a_bytes / pow(1024, 3), 2) . ' GB';
    } elseif ($a_bytes < pow(1024, 5)) {
        return round($a_bytes / pow(1024, 4), 2) . ' TB';
    }
}