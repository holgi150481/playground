<?php
include('include/functions.php');
$mounts = getFolders();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="iso-8859-1">
        <title>Show HDS Stats</title>
        <script src="js/raphael.2.1.0.min.js"></script>
        <script src="js/justgage.1.0.1.js"></script>
        <script src="js/gauge.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="css/styles.css" />
    </head>
    <body>
        <div class="wrapper">
            <div class="headline">
                <h1>Festplatten &Uuml;bersicht</h1>
                <h3>server.howdynet.local</h3>
            </div>
            <div class="total">
                <canvas width="300" height="300"
                    data-type="canv-gauge"
                    data-title="HD Gesamt"
                    data-min-value="0"
                    data-max-value="100"
                    data-value-format="2,0"
                    data-value="<?php echo $mounts['total']['percent']; ?>"
                    data-colors-needle="#FF0000 #FF0000"
                    data-highlights="0 20 #F2F2F2,20 40 #BDBDBD,40 60 #848484, 60 80 #585858, 80 100 #F78181"
                ></canvas>
            </div>
            <div class="total_info">
                // Gr&ouml;&szlig;e:  <?php echo format_bytes($mounts['total']['total']); ?> //
                Genutzt: <?php echo format_bytes($mounts['total']['used']); ?> //
                Frei: <?php echo format_bytes($mounts['total']['free']); ?> //
            </div>
            <div class="content">
                <?php
                    foreach($mounts as $mount) {
                        if (empty($mount['folder']) || $mount['folder'] == '/media/') continue;
                        ?>
                        <div class="entry">
                            <div id="<?php echo $mount['folder']; ?>"></div>
                            <div class="details">
                                <table>
                                    <tr>
                                        <td>Gr&ouml;&szlig;e:</td>
                                        <td><?php echo format_bytes($mount['total']); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Genutzt:</td>
                                        <td><?php echo format_bytes($mount['used']); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Frei:</td>
                                        <td><?php echo format_bytes($mount['free']); ?></td>
                                    </tr>
                                </table>
                            </div>
                            <script>
                                var g = new JustGage({
                                    id: "<?php echo $mount['folder']; ?>",
                                    value: <?php echo $mount['percent'];  ?>,
                                    min: 0,
                                    max: 100,
                                    title: "<?php echo $mount['folder']; ?>"
                                });
                            </script>
                        </div>
                <?php } ?>
            </div>
        </div>
    </body>
</html>