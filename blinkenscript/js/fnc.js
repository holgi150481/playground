var movieData;
var counter = 1;
var xhr = xmlhttp();
var aktiv;

// Film starten und importieren
function startMovie() {
	document.getElementById('start').disabled = true;
	var movie = document.getElementById('movie').value
	if (movie == 'debug') {
		debugMovie();
	} else {
		callServer(movie);
	}
}

// Film stoppen
function stopMovie() {
	window.clearInterval(aktiv);
	document.getElementById('start').disabled = false;
	counter = 1;
	clear();		
}

// Feld leeren
function clear() {
	for (var i=0; i <= 143; i++) {
		document.getElementById(i).src = "img/bulp_off.png";
	}
	document.getElementById('frame').innerHTML = "";
}

// XML Request objekt erstellen
function xmlhttp() {
	var xhr;
	if (window.XMLHttpRequest) {
		xhr = new XMLHttpRequest(); // IE 7+, alle anderen Browser
	} else if (window.ActiveXObject) {
		xhr = new ActiveXObject("Microsoft.XMLHTTP"); // IE 5-6
	}
 	return xhr;
}

// PHP Datei rufen
function callServer (file) {
	xhr.open("POST", "import.php", true);
	xhr.onreadystatechange = getData;
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
	xhr.send("file=" + file);	
}

// Film als JSON Array empfangen und Intervall starten
function getData() {
	if (xhr.readyState == 4) { 
		movieData = eval("(" + xhr.responseText + ")");
		aktiv = window.setInterval("playMovie();", 150);
	}	
}

// Film abspielen
function playMovie() {
	writeFrame(movieData[counter]);	
	document.getElementById('frame').innerHTML = "Bild: " + counter + " / " + movieData.length;
	counter++;  		
	if (counter == movieData.length) {		
		stopMovie();
	}
}

// Frame ausgeben
function writeFrame(frame) {	
	var data = frame.slice(3);	
	for (var i=0; i <= 143; i++) {
		if (data[i] == 1)  {
			document.getElementById(i).src = "img/bulp_on.png";			
		} else {
			document.getElementById(i).src = "img/bulp_off.png";
		}
	}	
}