function xmlhttp() {
	var xhr; 	
 	if (window.XMLHttpRequest) {
 		xhr = new XMLHttpRequest(); // IE 7+, alle anderen Browser
 	} else if (window.ActiveXObject) {
 		xhr = new ActiveXObject("Microsoft.XMLHTTP"); // IE 5-6
 	} 	 	
 	return xhr;
}

var xhr = xmlhttp(); 

function rufeServer () {	
	xhr.open("POST", "write.php", true);
	xhr.onreadystatechange = gibDatenaus;
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
	xhr.send(
		"text001=" + escape(document.getElementById("text001").value) +
		"&text002=" + escape(document.getElementById("text002").value) +
		"&text005=" + escape(document.getElementById("text005").value) +
		"&text010=" + escape(document.getElementById("text010").value) + 
		"&text020=" + escape(document.getElementById("text020").value) +
		"&text050=" + escape(document.getElementById("text050").value) +
		"&text100=" + escape(document.getElementById("text100").value) +
		"&text200=" + escape(document.getElementById("text200").value) +
		"&text500=" + escape(document.getElementById("text500").value) +
		"&text1000=" + escape(document.getElementById("text1000").value) +
		"&text2000=" + escape(document.getElementById("text2000").value) +
		"&text5000=" + escape(document.getElementById("text5000").value) +
		"&text10000=" + escape(document.getElementById("text10000").value) +
		"&text20000=" + escape(document.getElementById("text20000").value) +
		"&text50000=" + escape(document.getElementById("text50000").value) +
		"&controlText=" + escape(document.getElementById("controlText").value) 
		);
}
	
function gibDatenaus() {
	if (xhr.readyState == 4) {
		cleanForm();
		alert("Daten wurden gespeichert.")		
	}
}