<?php
require_once('include/config.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1" />
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<script type="text/javascript" src="js/fnc.js"></script>
		<script type="text/javascript" src="js/ajax.js"></script>		
		<title>Z�hlhilfe</title>
	</head>
	<body>
		<h1>..:: Z�hlhilfe ::..</h1>
		<table border="0" cellspacing="10px">
			<tr>
				<th colspan="3" style="font-size: 1.5em;">M�nzen</th>
				<th colspan="3" style="font-size: 1.5em;">Scheine</th>
			</tr>
			<tr>
				<th>Anzahl</th>
				<th>Art</th>
				<th>Summe</th>
				<th>Anzahl</th>
				<th>Art</th>
				<th>Summe</th>
			</tr>
			<tr> 
				<td>
					<input name="coinCount" type="text" id="text001" value="0" tabindex="1" onchange="countMoneyfromText(this, '001', '.01');" size="5">
					<button onclick="countMoney('001', 'down', 'coin', '.01');" tabindex="-1">-</button>
					<button onclick="countMoney('001', 'up', 'coin', '.01');" tabindex="-1">+</button>
				</td>
				<td>0.01 &euro;</td>
				<td name="coinTotal" id="currency001">0.00 &euro;</td>
				<td>
					<input name="noteCount" type="text" id="text500" value="0" tabindex="9" onchange="countMoneyfromText(this, '500', '5');" size="5">
					<button onclick="countMoney('500', 'down', 'note', '5');" tabindex="-1">-</button>
					<button onclick="countMoney('500', 'up', 'note', '5');" tabindex="-1">+</button>
				</td>
				<td>5.00 &euro;</td>
				<td name="noteTotal" id="currency500">0.00 &euro;</td>
			</tr>
				<tr>
				<td>
					<input name="coinCount" type="text" id="text002" value="0" tabindex="2" onchange="countMoneyfromText(this, '002', '.02');" size="5">
					<button onclick="countMoney('002', 'down', 'coin', '.02');" tabindex="-1">-</button>
					<button onclick="countMoney('002', 'up', 'coin', '.02');" tabindex="-1">+</button>
				</td>
				<td>0.02 &euro;</td>
				<td name="coinTotal" id="currency002">0.00 &euro;</td>
				<td>
					<input name="noteCount" type="text" id="text1000" value="0" tabindex="10" onchange="countMoneyfromText(this, '1000', '10');" size="5">
					<button onclick="countMoney('1000', 'down', 'note', '10');" tabindex="-1">-</button>
					<button onclick="countMoney('1000', 'up', 'note', '10');" tabindex="-1">+</button>
				</td>
				<td>10.00 &euro;</td>
				<td name="noteTotal" id="currency1000">0.00 &euro;</td>
			</tr>
			<tr>
				<td>
					<input name="coinCount" type="text" id="text005" value="0" tabindex="3" onchange="countMoneyfromText(this, '005', '.05');" size="5">
					<button onclick="countMoney('005', 'down', 'coin', '.05');" tabindex="-1">-</button>
					<button onclick="countMoney('005', 'up', 'coin', '.05');" tabindex="-1">+</button></td>
				<td>0.05 &euro;</td>
				<td name="coinTotal" id="currency005">0.00 &euro;</td>
				<td>
					<input name="noteCount" type="text" id="text2000" value="0" tabindex="11" onchange="countMoneyfromText(this, '2000', '20');" size="5">
					<button onclick="countMoney('2000', 'down', 'note', '20');" tabindex="-1">-</button>
					<button onclick="countMoney('2000', 'up', 'note', '20');" tabindex="-1">+</button>
				</td>
				<td>20.00 &euro;</td>
				<td name="noteTotal" id="currency2000">0.00 &euro;</td>
			</tr>
			<tr>
				<td>
					<input name="coinCount" type="text" id="text010" value="0" tabindex="4" onchange="countMoneyfromText(this, '010', '.1');" size="5">
					<button onclick="countMoney('010', 'down', 'coin', '.1');" tabindex="-1">-</button>
					<button onclick="countMoney('010', 'up', 'coin', '.1');" tabindex="-1">+</button>
				</td>
				<td>0.10 &euro;</td>
				<td name="coinTotal" id="currency010">0.00 &euro;</td>
				<td>
					<input name="noteCount" type="text" id="text5000" value="0" tabindex="13" onchange="countMoneyfromText(this, '5000', '50');" size="5">
					<button onclick="countMoney('5000', 'down', 'note', '50');" tabindex="-1">-</button>
					<button onclick="countMoney('5000', 'up', 'note', '50');" tabindex="-1">+</button></td>
				<td>50.00 &euro;</td>
				<td name="noteTotal" id="currency5000">0.00 &euro;</td>
			</tr>
			<tr>
				<td><input name="coinCount" type="text" id="text020" value="0" tabindex="5" onchange="countMoneyfromText(this, '020', '.2');" size="5">
					<button onclick="countMoney('020', 'down', 'coin', '.2');" tabindex="-1">-</button>
					<button onclick="countMoney('020', 'up', 'coin', '.2');" tabindex="-1">+</button></td>
				<td>0.20 &euro;</td>
				<td name="coinTotal" id="currency020">0.00 &euro;</td>
				<td>
					<input name="noteCount" type="text" id="text10000" value="0" tabindex="14" onchange="countMoneyfromText(this, '10000', '100');" size="5">
					<button onclick="countMoney('10000', 'down', 'note', '100');" tabindex="-1">-</button>
					<button onclick="countMoney('10000', 'up', 'note', '100');" tabindex="-1">+</button></td>
				<td>100.00 &euro;</td>
				<td name="noteTotal" id="currency10000">0.00 &euro;</td>
			</tr>
			<tr>
				<td>
					<input name="coinCount" type="text" id="text050" value="0" tabindex="6" onchange="countMoneyfromText(this, '050', '.5');" size="5">
					<button onclick="countMoney('050', 'down', 'coin', '.50');" tabindex="-1">-</button>
					<button onclick="countMoney('050', 'up', 'coin', '.50');" tabindex="-1">+</button>
				</td>
				<td>0.50 &euro;</td>
				<td name="coinTotal" id="currency050">0.00 &euro;</td>
				<td>
					<input name="noteCount" type="text" id="text20000" value="0" tabindex="15" onchange="countMoneyfromText(this, '20000', '200');" size="5">
					<button onclick="countMoney('20000', 'down', 'note', '200');" tabindex="-1">-</button>
					<button onclick="countMoney('20000', 'up', 'note', '200');" tabindex="-1">+</button>					
				</td>
				<td>200.00 &euro;</td>
				<td name="noteTotal" id="currency20000">0.00 &euro;</td>
			</tr>
			<tr>
				<td>
					<input name="coinCount" type="text" id="text100" value="0" tabindex="7" onchange="countMoneyfromText(this, '100', '1');" size="5">
					<button onclick="countMoney('100', 'down', 'coin', '1');" tabindex="-1">-</button>
					<button onclick="countMoney('100', 'up', 'coin', '1');" tabindex="-1">+</button>
				</td>
				<td>1.00 &euro;</td>
				<td name="coinTotal" id="currency100">0.00 &euro;</td>
				<td>
					<input name="noteCount" type="text" id="text50000" value="0" tabindex="16" onchange="countMoneyfromText(this, '50000', '500');" size="5">
					<button onclick="countMoney('50000', 'down', 'note', '500');" tabindex="-1">-</button>
					<button onclick="countMoney('50000', 'up', 'note', '500');" tabindex="-1">+</button>
				</td>
				<td>500.00 &euro;</td>
				<td name="noteTotal" id="currency50000">0.00 &euro;</td>
			</tr>
			<tr>
				<td>
					<input name="coinCount" type="text" id="text200" value="0" tabindex="8" onchange="countMoneyfromText(this, '200', '2');" size="5">
					<button onclick="countMoney('200', 'down', 'coin', '2');" tabindex="-1">-</button>
					<button onclick="countMoney('200', 'up', 'coin', '2');" tabindex="-1">+</button>
				</td>
				<td>2.00 &euro;</td>
				<td name="coinTotal" id="currency200">0.00 &euro;</td>
			</tr>
			<tr>
				<td colspan="2">Anzahl M�nzen:</td>
				<td id="countCoins">0</td>
				<td colspan="2">Anzahl Scheine:</td>
				<td id="countNotes">0</td>
			<tr>
				<td colspan="2">Wert M�nzen:</td>
				<td id="amountCoins">0.00 &euro;</td>
				<td colspan="2">Wert Scheine:</td>
				<td id="amountNotes">0.00 &euro;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">Anzahl M�nzen/Scheine:</td>
				<td id="countAll"><strong>0</strong></td>
				<td colspan="2">Gesamt Betrag:</td>
				<td id="amountAll"><strong>0.00 &euro;</strong></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="1">Kontroll Betrag:</td>
				<td colspan="2"><input type="text" id="" value="0.00" size="13" onchange="setControlValue(this);" tabindex="-1"></td>
				<td colspan="2">Differenz Betrag:</td>
				<td id="controlValue">0.00 &euro;</td>
			</tr>
		</table>
		<table>
			<tr>
				<td><button>Speichern</button></td>				
				<td><button onclick="window.location.href='read.php';">Liste</button></td>
			</tr>
		</table>
	</body>
</html>
