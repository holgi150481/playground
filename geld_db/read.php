<?php
require_once('include/config.php');

$sql = "SELECT * from " . TAB_GELD_DB;
$sql = $db->query($sql)

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Geldz�hler - Liste</title>	
	</head>
	<body>
		<?php while ($line = $sql->fetch_array()) { ?>				
			<table border="1"> 
				<tr>
					<td>Datum:</td>
					<td colspan="3"><?php echo $line['datum']; ?></td>
					<td>&nbsp;</td>
					<td colspan="2">M�nzen:</td>
					<td><?php $coins = $line['001'] + $line['002'] + $line['005'] + $line['010'] + $line['020'] + $line['050'] + $line['100'] + $line['200']; echo $coins; ?></td>					
					<td>&nbsp;</td>
					<td colspan="2">Scheine:</td>
					<td><?php $notes = $line['500'] + $line['1000'] + $line['2000'] + $line['5000'] + $line['10000'] + $line['20000'] + $line['50000']; echo $notes; ?></td>				
					<td>&nbsp;</td>
					<td colspan="2">Gesamt:</td>
					<td><?php echo $coins + $notes; ?></td>
				</tr>
				<tr>
					<td>Art:</td>
					<td>0.01 &euro;</td>
					<td>0.02 &euro;</td>
					<td>0.05 &euro;</td>
					<td>0.10 &euro;</td>
					<td>0.20 &euro;</td>
					<td>0.50 &euro;</td>
					<td>1.00 &euro;</td>
					<td>2.00 &euro;</td>				
					<td>5.00 &euro;</td>
					<td>10.00 &euro;</td>
					<td>20.00 &euro;</td>
					<td>50.00 &euro;</td>
					<td>100.00 &euro;</td>
					<td>200.00 &euro;</td>				 				
					<td>500.00 &euro;</td>
				</tr>
				<tr>
					<td>Anzahl:</td>
					<td><?php echo $line['001'] ?></td>
					<td><?php echo $line['002'] ?></td>
					<td><?php echo $line['005'] ?></td>
					<td><?php echo $line['010'] ?></td>
					<td><?php echo $line['020'] ?></td>
					<td><?php echo $line['050'] ?></td>
					<td><?php echo $line['100'] ?></td>
					<td><?php echo $line['200'] ?></td>
					<td><?php echo $line['500'] ?></td>
					<td><?php echo $line['1000'] ?></td>
					<td><?php echo $line['2000'] ?></td>
					<td><?php echo $line['5000'] ?></td>
					<td><?php echo $line['10000'] ?></td>
					<td><?php echo $line['20000'] ?></td>
					<td><?php echo $line['50000'] ?></td>
				</tr>
				<tr>
					<td>value:</td>
					<td><?php $value001 = $line['001'] * .01; echo sprintf("%.2f", $value001) ?> &euro;</td>
					<td><?php $value002 = $line['002'] * .02; echo sprintf("%.2f", $value002) ?> &euro;</td>
					<td><?php $value005 = $line['005'] * .05; echo sprintf("%.2f", $value005) ?> &euro;</td>
					<td><?php $value010 = $line['010'] * .10; echo sprintf("%.2f", $value010) ?> &euro;</td>
					<td><?php $value020 = $line['020'] * .20; echo sprintf("%.2f", $value020) ?> &euro;</td>
					<td><?php $value050 = $line['050'] * .50; echo sprintf("%.2f", $value050) ?> &euro;</td>
					<td><?php $value100 = $line['100'] * 1; echo sprintf("%.2f", $value100) ?> &euro;</td>
					<td><?php $value200 = $line['200'] * 2; echo sprintf("%.2f", $value200) ?> &euro;</td>
					<td><?php $value500 = $line['500'] * 5; echo sprintf("%.2f", $value500) ?> &euro;</td>
					<td><?php $value1000 = $line['1000'] * 10; echo sprintf("%.2f", $value1000) ?> &euro;</td>
					<td><?php $value2000 = $line['2000'] * 20; echo sprintf("%.2f", $value2000) ?> &euro;</td>
					<td><?php $value5000 = $line['5000'] * 50; echo sprintf("%.2f", $value5000) ?> &euro;</td>
					<td><?php $value10000 = $line['10000'] * 100; echo sprintf("%.2f", $value10000) ?> &euro;</td>
					<td><?php $value20000 = $line['20000'] * 200; echo sprintf("%.2f", $value20000) ?> &euro;</td>
					<td><?php $value50000 = $line['50000'] * 500; echo sprintf("%.2f", $value50000) ?> &euro;</td>
				</tr>
				<tr>
					<td colspan="3">Wert M�nzen:</td>
					<td colspan="2"><?php $valueCoins = $value001 + $value002 + $value005 + $value010 + $value020 + $value050 + $value100 + $value200; echo sprintf("%.2f", $valueCoins); ?> &euro;</td>				
					<td colspan="3">Wert Scheine:</td>
					<td colspan="2"><?php $valueNotes = $value500 + $value1000 + $value2000 + $value5000 + $value10000 + $value20000 + $value50000; echo sprintf("%.2f", $valueNotes); ?> &euro;</td>		
					<td>&nbsp;</td>		
					<td colspan="3">Wert Gesamt:</td>				
					<td colspan="2"><?php echo sprintf("%.2f", $valueCoins + $valueNotes) ?> &euro;</td>
				</tr>
			</table>
			<br />
		<?php } ?>
	</body>
</html>