<?php
/**
 * Debugging Klasse
 * @version 0.5
 * @author HH
 **/

class debug {
	
	private static $errorMessage;	
	
	/**
	 * Debug Funktion ein- oder ausschalten
	 * 
	 * @param $debug boolean
	 **/
	public static function errorHandling($var = FALSE) {
		self::$errorMessage = $var;		
		if (self::$errorMessage) {
			error_reporting(E_ALL | E_STRICT);
			ini_set('error_prepend_string', '<pre style="font-size: 1.2em; color: red;">');
			ini_set('error_append_string', '</pre>');
			 			
		}
		return self::$errorMessage;
	}

	/**
	 * Gibt einen formatierten Backtrace aus.
	 **/
	public static function backtrace() {
		foreach (debug_backtrace(FALSE) as $key => $value) {
			$klasse = (!empty($value['class']) ? ' <strong>Klasse:</strong> ' . $value['class'] : "");
			echo '<strong>Datei:</strong> ' . $value['file'] . 
			' <strong>Funktion:</strong> ' . $value['function'] .	
			$klasse . 
			' <strong>Zeile:</strong> ' . $value['line'] . '<br/>';		
		}
	}
	
	/**
	 * Ein Array formatiert ausgeben
	 *  
	 * @param $var Variable oder Array.
	 **/
	public static function output($var) {		
		$backtrace = debug_backtrace(FALSE);
		echo '<pre>Aufruf: ' . $backtrace[0]['file'] . ' In Zeile: '. $backtrace[0]['line'] .  '</pre>';
		foreach ($GLOBALS as $key => $value) {
			if($value === $var) {
				$ident = $key;
			} 
		}
		echo '<pre>Variable: ' . (!empty($ident) ? '&#36;' . $ident : "Unbekannt")  . '</pre>';
		if (is_array($var)) {
			echo '<pre>' . print_r($var, TRUE) . '</pre>';	
		} else {
			echo '<pre>' . var_dump($var) . '</pre>';
		}	 
	}
	 
}
?>
