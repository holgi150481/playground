<?php
include('debug.class.php');
include('functions.php');

// DB Zugangsdaten
define('DB_HOST', 'localhost');
define('DB_USER', 'phpuser');
define('DB_PASS', 'secret');
define('DB_NAME', 'sqltest');

// DB Tabellen
define('TAB_GELD_DB', 'geld_db');

// DB Verbinden
$db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

?>
