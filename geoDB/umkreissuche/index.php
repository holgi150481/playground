<?php
$db = new mysqli('localhost', 'phpuser', 'secret', 'geodb');

if (isset($_REQUEST['search'])) {
    // Ausgangspunkt
    $sql =  ' SELECT zc_id, zc_location_name, zc_lat, zc_lon, zc_zip' .
            ' FROM zip_coordinates' .
            ' WHERE zc_zip=' . (int)$_REQUEST['plz'] . ' LIMIT 1';
    $query = $db->query($sql);
    $location = $entry = $query->fetch_assoc();

    // Umkreissuche
    $sql =  ' SELECT dest.zc_zip, dest.zc_location_name,' .
            ' ACOS(' .
                ' SIN(RADIANS(src.zc_lat)) * SIN(RADIANS(dest.zc_lat))' .
                ' + COS(RADIANS(src.zc_lat)) * COS(RADIANS(dest.zc_lat))' .
                ' * COS(RADIANS(src.zc_lon) - RADIANS(dest.zc_lon))' .
            ' ) * 6380 AS distance' .
            ' FROM zip_coordinates dest' .
            ' CROSS JOIN zip_coordinates src' .
            ' WHERE src.zc_id = ' . $location['zc_id'] . ' AND dest.zc_id <> src.zc_id'.
            ' HAVING distance < ' . (int)$_REQUEST['radius'] .
            ' ORDER BY distance';
    $query = $db->query($sql);
    while ($row = $query->fetch_assoc()) {
        $data[] = $row;
    }

} ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="ISO-8859-1" />
        <title>Umkreissuche</title>
        <style>
            label {
                display: inline-block;
                width: 150px;
                padding: 5px;
            }
            table {
                width: 50%;
            }
            th {
                text-align: left;
            }
        </style>
    </head>

    <body>
        <h1>..:: Umkreissuche ::..</h1>

        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <label for="plz">Postleitzahl</label>
            <input type="text" id="plz" name="plz" value="<?php echo isset($_REQUEST['plz']) ? $_REQUEST['plz'] : '26639'; ?>"/>
            <br>
            <label for="radius">Entfernung in km</label>
            <input type="text" id="radius" name="radius" value="<?php echo isset($_REQUEST['radius']) ? $_REQUEST['radius'] : '15'; ?>"/>
            <br>
            <input type="submit" name="search" value="Suchen"/>
        </form>

        <?php if (!empty($location)): ?>
            <br><hr><br>
            Ausgangsort: <span style="font-weight: bold;"><?php echo $location['zc_zip'] . ' ' . $location['zc_location_name']; ?></span>
            <br><br>
            <?php if (!empty($data)) { ?>
                <table>
                    <tr>
                        <th>PLZ</th>
                        <th>Ort</th>
                        <th>Entfernung</th>
                    </tr>
                    <?php foreach($data as $loc): ?>
                        <tr>
                            <td><?php echo $loc['zc_zip']; ?></td>
                            <td><?php echo $loc['zc_location_name']; ?></td>
                            <td><?php echo round($loc['distance'], 2); ?> km</td>
                        </tr>
                    <?php endforeach ?>
                </table>
            <?php } else { ?>
                <p>Keine Ort im Umkreis von <?php echo $_REQUEST['radius']; ?> km gefunden.</p>
            <?php } ?>
        <?php endif ?>
    </body>
</html>