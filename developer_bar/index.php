<?php
		
	require_once('dev_bar/include.php');
	
	$bool = true;
	vardump($bool);
		
	vardump("test");
	vardump($_SERVER['REQUEST_TIME']);
	vardump($_FILES); 
	
	echo $test;
	
	trigger_error('Test 1');
	trigger_error('Test 2', E_USER_ERROR);
	trigger_error('Test Fehlermeldung 3', E_USER_DEPRECATED);	
	
?>
<!DOCTYPE html>
<html>
	<head>		
		<meta charset="ISO-8859-1"/>
		<?php dev_bar_init(); ?>
		<title>SQL lesen</title>
	</head>
	<body>
		<div onclick="jslog('test Log...')">-> jslog test <-</div>
		<?php
		$db = new MySQLi("localhost", "phpuser", "secret", "sqltest");
		
		$sql = 'SELECT * from data';
		$ergebnis = $db->query($sql);
		sqldump($sql);
		
		echo '<table border="1">';
		echo '<tr><th>id</th><th>name</th><th>str</th><th>plz</th><th>ort</th></tr>';
		
		while ($zeile = $ergebnis->fetch_array()) {
			echo '<tr><td>' . htmlspecialchars($zeile['id']) . '</td>'
			. '<td>' . htmlspecialchars($zeile['name']) . '</td>'
			. '<td>' . htmlspecialchars($zeile['str']) . '</td>'
			. '<td>' . htmlspecialchars($zeile['plz']) . '</td>'
			. '<td>' . htmlspecialchars($zeile['ort']) . '</td></tr>';
		}
		
		echo '</table>';
		
		?>
		<?php dev_bar_show(); ?>
	</body>
</html>

