var jslog_counter = 0;

// Elemente Anzeigen oder Vestecken
function showBox(element, group) {
	var frames = document.getElementsByClassName(group);
	var show_frame = document.getElementById(element);
	for (var i=0; i < frames.length; i++) {
		if (frames[i] == show_frame) {
			if(show_frame.style.display == 'none') {
    			show_frame.style.display = 'block';
    		} else {
    			show_frame.style.display = 'none';
			}			
		} else {
			frames[i].style.display = 'none';
		}	 
	}
}

// Tabellen Zeilen bei Klick einf�rben
function colorRow(row) {
	if (row.className == 'dev_bar_table_row') {
		row.className = 'dev_bar_table_row_selected';
	} else {
		row.className = 'dev_bar_table_row';
	}
}

// Eintr�ge in JavaScript Log erstellen
function jslog(message) {	
	jslog_counter++;
	var dummy = document.getElementById('js_log_dummy');
	dummy.style.display = 'none';
	
	var bar_text = document.getElementById('dev_bar_text_jslog');
	bar_text.style.color = 'blue';
	bar_text.innerHTML = 'JavaScript Log (' + jslog_counter + ')';
	
	var box = document.getElementById('js_log');
	var newSpan = document.createElement("span");
	var content = document.createTextNode(jslog_counter + ' ' + message);
	newSpan.appendChild(content);	
	box.appendChild(newSpan);
	
	var newBR = document.createElement("br");
	box.appendChild(newBR);
}