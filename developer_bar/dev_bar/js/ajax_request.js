function xmlhttp() {
	var xhr;
	if (window.XMLHttpRequest) {
		xhr = new XMLHttpRequest(); // IE 7+, alle anderen Browser
	} else if (window.ActiveXObject) {
		xhr = new ActiveXObject("Microsoft.XMLHTTP"); // IE 5-6
	}
	return xhr;
} 

var xhr = xmlhttp(); 

function dev_bar_ajaxRequest(query, type) {
	if (type == 'sql') {
		xhr.open("POST", "dev_bar/ajax/sql.php", true) // Datei �ffnen	
	}
	xhr.onreadystatechange = dev_bar_getData; // Wenn Daten eingehen funktion aufrufen
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // Request header zum senden der Anfrage setzen!!
	xhr.send("query=" + query); // Anfage absenden
}

function dev_bar_getData() {
	if (xhr.readyState == 4) { 
		var ergebnis = xhr.responseText; 
		document.getElementById("dev_bar_ajaxRequest").innerHTML = ergebnis
		showBox('dev_bar_ajaxRequest', 'dev_frame');
	}
} 