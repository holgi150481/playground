<?php

function dev_bar_show_options() { 
	if (isset($_COOKIE['dev_bar_eMail'])) {
		$mail = $_COOKIE['dev_bar_eMail'];
	} else {
		$mail = DEV_BAR_MAIL;
	}
	
	?>
	<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" class="dev_options">
		<input type="hidden" name="back_to" value="<?php echo $_SERVER['PHP_SELF']; ?>" />
		<div>				
			<label for="dev_bar_position">Position: fixed</label>
			<input type="checkbox"name="dev_bar_position"/>
		</div>
		<div>
			<label>Bar Farbe</label>
			<select name="dev_bar_color_bar">
				<option value="#EEEEEE" style="background-color: #EEEEEE;"></option>
				<option value="#AAEEEE" style="background-color: #AAEEEE;"></option>
				<option value="#AABBEE" style="background-color: #AABBEE;"></option>
			</select>
		</div>
		<div>
			<label>Frame Farbe</label>
			<select name="dev_bar_color_bar_frame">
				<option value="#EEEEEE" style="background-color: #EEEEEE;"></option>
				<option value="#AAEEEE" style="background-color: #AAEEEE;"></option>
				<option value="#AABBEE" style="background-color: #AABBEE;"></option>
			</select>			
		</div>
		<div>
			<label>eMail Empf�nger</label>
			<input type="email" name="dev_bar_eMail" value="<?php echo $mail; ?>"/>
		</div>
		<div>
			<input type="submit" name="dev_bar_save_options" value="Speichern"/>
		</div>		
	</form>	
<?php } 

?>