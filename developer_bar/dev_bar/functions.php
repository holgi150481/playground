<?php
function vardump($var) {
	global $dump_var;
	$backtrace = debug_backtrace(FALSE);
	$output =  '<pre>Aufruf: <strong>' . $backtrace[0]['file'] . '</strong> In Zeile: <strong>'. $backtrace[0]['line'] .  '</strong></pre>';
	foreach ($GLOBALS as $key => $value) {
		if($value === $var) {
			$varName = $key;
		} 
	}        
	$output .= '<pre>Variable: <strong>' . (!empty($varName) ? '&#36;' . $varName : "Unbekannt")  . '</strong></pre>';
	if (is_array($var) || is_object($var)) {
		$output .= '<pre>' . print_r($var, TRUE) . '</pre>';	
	} else {
	    ob_start();
        echo '<pre>' . var_dump($var) . '</pre>';
		$output .= ob_get_contents();
        ob_end_clean();
	}		
    $dump_var[] = $output;
} 

function sqldump($sql) {
	global $dump_sql;
	$backtrace = debug_backtrace(FALSE);
	$output['output'] =  '<pre>Aufruf: <strong>' . $backtrace[0]['file'] . '</strong> In Zeile: <strong>'. $backtrace[0]['line'] .  '</strong><br />';
	$output['output'] .= $sql . '</pre>';
	$output['query'] = $sql;
    $dump_sql[] = $output;	
}

function dev_bar_error_handler($errorCode, $errorMsg, $errorFile, $errorLine) {
	global $dev_bar_exceptions;

	switch ($errorCode) {
    	case E_USER_ERROR:
			$errorInfo = "Benutzer Fehler";		
	        break;

    	case E_USER_NOTICE:
			$errorInfo = "Benutzer Nachricht";		
	        break;
			
		case E_NOTICE:
			$errorInfo = "Benachrichtigung";		
	        break;

	    default:
    		return false;	    	
    	    break;
    }
		
	$dev_bar_exceptions[] = array(
		'errorCode' 	=> $errorCode,
		'errorMsg' 		=> $errorMsg,
		'errorFile' 	=> $errorFile,
		'errorLine' 	=> $errorLine,
		'errorInfo'		=> $errorInfo				
	);
	
	// PHP Fehlermeldung unterdrücken!
    return true;
}

?>