<?php

function dev_bar_show_vardump() {
	global $dump_var;
	if (!empty($dump_var)) {		
		foreach ($dump_var as $key => $value) {
			echo '<pre>' . print_r($value, true) . '</pre>';
			echo '<hr class="dev_bar_hr" />';	
		}		
	} else {
		echo "<pre><strong>vardump()</strong> Wurde nicht ausgef&uuml;hrt!</pre>";
	} 
} 

function dev_bar_show_globals() { ?>
	<div class="dev_link" onclick="showBox('var_dump_globals_sever', 'var_dump_globals');">$_SERVER</div>
	<div style="display: none;" class="var_dump_globals" id="var_dump_globals_sever">
		<?php echo '<pre>' . print_r($_SERVER, true) . '</pre>'; ?>
	</div>
	
	<div class="dev_link" onclick="showBox('var_dump_globals_request', 'var_dump_globals');">$_REQUEST</div>
	<div style="display: none;" class="var_dump_globals" id="var_dump_globals_request">
		<?php echo '<pre>' . print_r($_REQUEST, true) . '</pre>'; ?>
	</div>
	
	<div class="dev_link" onclick="showBox('var_dump_globals_session', 'var_dump_globals');">$_SESSION</div>
	<div style="display: none;" class="var_dump_globals" id="var_dump_globals_session">
		<?php if (!empty($_SESSION)) {
			echo '<pre>' . print_r($_SESSION, true) . '</pre>';
		} else {
			echo "<pre>Sessions sind nicht aktiv!</pre>";
		} ?>
	</div>
	
	<div class="dev_link" onclick="showBox('var_dump_globals_cookie', 'var_dump_globals');">$_COOKIE</div>
	<div style="display: none;" class="var_dump_globals" id="var_dump_globals_cookie">
		<?php echo '<pre>' . print_r($_COOKIE, true) . '</pre>'; ?>
	</div>
	
	<div class="dev_link" onclick="showBox('var_dump_globals_env', 'var_dump_globals');">$_ENV</div>
	<div style="display: none;" class="var_dump_globals" id="var_dump_globals_env" >
		<?php echo '<pre>' . print_r($_ENV, true) . '</pre>'; ?>
	</div>		
<?php } 

function dev_bar_show_sql_dump() {
	global $dump_sql;
	if (!empty($dump_sql)) {
		foreach ($dump_sql as $key => $value) {
			echo "<pre class='dev_link' onclick=\"dev_bar_ajaxRequest('" . $value['query'] . "', 'sql');\">" . $value['output'] . "</pre>";
			echo '<hr class="dev_bar_hr" />';	
		}
	} else {
		echo "<pre><strong>sqldump()</strong> Wurde nicht ausgef&uuml;hrt!</pre>";
	} 
} 
	
function dev_bar_show_exceptions() {
	global $dev_bar_exceptions;
	if (empty($dev_bar_exceptions)) {
		echo "<pre>Keine Fehler aufgetreten.</pre>";		
	} else {				
		echo '<pre>';
		echo '<div class="dev_error_frame_header">Es sind ' . count($dev_bar_exceptions) . ' Fehler aufgetreten!</div>';
		foreach ($dev_bar_exceptions as $key => $value) {			
			echo '<hr class="dev_bar_hr" />';	
			echo '<strong>' . $value['errorInfo'] . '</strong> Code:(' . $value['errorCode'] . ')<br />';
			echo 'Info: <strong>' . $value['errorMsg'] . '</strong><br />';
			echo 'Datei: <strong>' . $value['errorFile'] . '</strong>';
			echo ' In Zeile: <strong>' . $value['errorLine'] . '</strong><br />';
		}
		echo '</pre>';
		echo '<hr class="dev_bar_hr" />';	
	}
}

function dev_bar_show_source_code() {
	highlight_file(basename($_SERVER['PHP_SELF']));
} 

function dev_bar_show_dev_informations() {
	global $dev_bar_allowed_IPs, $dump_sql;
	echo '<pre>';
	
	echo '<strong>Backtrace:</strong><br />';
	foreach (debug_backtrace(FALSE) as $key => $value) {			
		echo '<strong>Datei:</strong> ' . $value['file']; 
		echo ' <strong>Funktion:</strong> ' . $value['function'];	
	 	echo (!empty($value['class']) ? ' <strong>Klasse:</strong> ' . $value['class'] : ""); 
		echo ' <strong>Zeile:</strong> ' . $value['line'] . '<br/>';            
	}
	
	if (DEV_BAR_SQL_ACTIVE) {
		global $db;
		
		echo '<br /><strong>MySQL Adresse und Protokoll:</strong><br />';
		echo $db->host_info . '<br />';
		
		echo '<br /><strong>MySQL Client Info:</strong><br />';
		echo $db->client_info . ' (' . $db->client_version . ')<br />';
		
		echo '<br /><strong>MySQL Server Info:</strong><br />';
		echo $db->server_info . ' (' . $db->server_version . ')<br />';
		
	}
	
	echo '<br /><strong>Web Server Info:</strong><br />';
	echo $_SERVER['SERVER_SIGNATURE'];
	
	echo '<strong>Client Info:</strong><br />';
	echo $_SERVER['HTTP_USER_AGENT'] . '<br />';
	
	echo '<br /><strong>Server Admin:</strong><br />';
	echo $_SERVER['SERVER_ADMIN'] . '<br />';
	
	echo '<br /><strong>Debug Mail Senden an:</strong><br />';
	echo DEV_BAR_MAIL . '<br />';
		
	echo '<br /><strong>Erlaubte IPs:</strong><br />';
	foreach ($dev_bar_allowed_IPs as $value) {
		echo $value . '<br />';
	}
	echo '</pre>'; 
} 

?>