<?php

if (!empty($_REQUEST['query'])) {
	
	require_once('../config.php');
	$db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	
	$sql = $_REQUEST['query'];
	$query = $db->query($sql);
	$tableInfo = $query->fetch_field();
	$fields = $query->fetch_fields();
	
	$explain = $db->query('EXPLAIN ' . $tableInfo->table);
			
	echo '<div class="dev_bar_header"><strong>' . $sql . '</strong></div>';
	echo 'Eintr&auml;ge:<strong> ' . $query->num_rows . '</strong><br />';
	echo 'Felder:<strong> ' . $query->field_count . '</strong><br />';
	echo 'Tabelle:<strong> ' . $tableInfo->table . '</strong><br />';
	echo 'Datenbank:<strong> ' . $tableInfo->db . '</strong><br /><br />';
	?>
	<table class="dev_bar_table">
		<tr class="dev_bar_table_head">
			<th>Feld</th>
			<th>Typ</th>
			<th>Allow Null</th>
			<th>Key</th>
			<th>Default</th>
			<th>Extra</th>			
		</tr> 
	<?php
	foreach ($explain as $key => $value) {
		echo '<tr class="dev_bar_table_row" onclick="colorRow(this);">';
			echo '<td>' . $value['Field'] . '</td>';
			echo '<td>' . $value['Type'] . '</td>';
			echo '<td>' . $value['Null'] . '</td>';
			echo '<td>' . $value['Key'] . '</td>';
			echo '<td>' . $value['Default'] . '</td>';
			echo '<td>' . $value['Extra'] . '</td>';
		echo '</tr>'; 
	}
	echo '</table>';
	echo '<br />';
	
	echo '<div class="dev_bar_table_container">';
	echo '<table class="dev_bar_table">'; 
	echo '<tr class="dev_bar_table_head">';
	foreach ($fields as $field) {
		echo '<th>' . $field->name . '</th>'; 
	}
	echo '</tr>';
	if ($query->num_rows) {
		while ($row = $query->fetch_array()) {
			echo '<tr class="dev_bar_table_row" onclick="colorRow(this);">';
			foreach ($fields as $field) {
				echo '<td>' . utf8_encode($row[$field->name]) . '</td>'; 
			}
			echo '</tr>';
		}
	}
	echo '</table>';
	echo '</div>';
	
} else {
	echo 'Keine Daten angegeben!';	
}
?>