<?php

// Einstellungen
//TODO eMail Ausgabe in dev_bar erstellen
define('DEV_BAR_MAIL', 'holgi@fafnir.ws');
define('DEV_BAR_COLOR_BAR', '#80ff80');
define('DEV_BAR_COLOR_FRAME', '#FFFF00');
define('DEV_BAR_COLOR_AJAX_FRAME', '#2EFEF7');

// SQL Einstellungen
define('DEV_BAR_SQL_ACTIVE', true);
define('DB_HOST', 'localhost');
define('DB_NAME', 'sqltest');
define('DB_USER', 'phpuser');
define('DB_PASS', 'secret');

$dev_bar_allowed_IPs = array(
	'192.168.0.1',
	'192.168.0.2'
);

?>