<?php

function dev_bar_init() {
	echo '<link type="text/css" rel="stylesheet" href="dev_bar/css/style.css" />';
	echo '<link type="text/css" rel="stylesheet" href="dev_bar/css/ajax.css" />';
	echo '<script type="text/javascript" src="dev_bar/js/ajax_request.js"></script>';	
	echo '<script type="text/javascript" src="dev_bar/js/fnc.js"></script>';	
}

function dev_bar_show() {
	global $dev_bar_exceptions, $dump_var, $dump_sql;
	?>
	<div class="dev_bar" id="dev_bar" style="background-color: <?php echo DEV_BAR_COLOR_BAR; ?>;">
		<span <?php echo (!empty($dump_var) ? 'style="color: blue;"' : ''); ?> class="dev_link" onclick="showBox('vardump', 'dev_frame');">Variablen Ausgabe<?php echo (!empty($dump_var) ? ' (' . count($dump_var) . ')' : ''); ?></span> |
		<span id="dev_bar_text_jslog" class="dev_link" onclick="showBox('js_log', 'dev_frame');">JavaScript Log</span> |
		<?php if (DEV_BAR_SQL_ACTIVE) { ?>
			<span <?php echo (!empty($dump_sql) ? 'style="color: blue;"' : ''); ?> class="dev_link" onclick="showBox('sql_dump', 'dev_frame');">SQL<?php echo (!empty($dump_sql) ? ' (' . count($dump_sql) . ')' : ''); ?></span> |
		<?php } ?>
		<span <?php echo (!empty($dev_bar_exceptions) ? 'style="color: red;"' : ''); ?> class="dev_link" onclick="showBox('exceptions', 'dev_frame');">Fehler<?php echo (!empty($dev_bar_exceptions) ? ' (' . count($dev_bar_exceptions) . ')' : ''); ?></span> |
		<span class="dev_link" onclick="showBox('globals', 'dev_frame');">Super Globals</span> |		
		<span class="dev_link" onclick="showBox('source_code', 'dev_frame');">Quell Code</span> |
		<span class="dev_link" onclick="showBox('dev_informations', 'dev_frame');">Informationen</span>
	</div>		
	<div class="dev_frame" id="vardump" style="display: none; background-color: <?php echo DEV_BAR_COLOR_FRAME; ?>;">
		<?php dev_bar_show_vardump(); ?>
	</div>
	
	<div class="dev_frame" id="js_log" style="display: none; background-color: <?php echo DEV_BAR_COLOR_FRAME; ?>;">
		<div id="js_log_dummy"><pre><strong>jslog()</strong> Wurde nicht ausgef&uuml;hrt!</pre></div>			
	</div>
		
	<?php if (DEV_BAR_SQL_ACTIVE) { ?>
		<div class="dev_frame" id="sql_dump" style="display: none; background-color: <?php echo DEV_BAR_COLOR_FRAME; ?>;">
			<?php dev_bar_show_sql_dump(); ?>
		</div>
	<?php } ?>
	
	<div class="dev_frame" id="exceptions" style="display: none; background-color: <?php echo DEV_BAR_COLOR_FRAME; ?>;">
		<?php dev_bar_show_exceptions(); ?>	
	</div>
	
	<div class="dev_frame" id="globals" style="display: none; background-color: <?php echo DEV_BAR_COLOR_FRAME; ?>;">
		<?php dev_bar_show_globals(); ?>
	</div>
	
	<div class="dev_frame" id="source_code" style="display: none; background-color: <?php echo DEV_BAR_COLOR_FRAME; ?>;">
		<?php dev_bar_show_source_code(); ?>
	</div>
	
	<div class="dev_frame" id="dev_informations" style="display: none; background-color: <?php echo DEV_BAR_COLOR_FRAME; ?>;">
		<?php dev_bar_show_dev_informations(); ?>
	</div>
	
	<div class="dev_frame" id="dev_bar_ajaxRequest" style="display: none; background-color: <?php echo DEV_BAR_COLOR_AJAX_FRAME; ?>;">
	</div>
	 
<?php }

?>