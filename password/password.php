<?php
class password {

	private $letters 	= 'abcdefghijklmnopqrstuvwxyz';
	private $signs 		= '!�$#+*-_.:,;/()[]{}^�@�';	

	private $length;
	private $useNumbers;
	private $useSigns;	
	private $caseSensitivity;

	private $password;

	public function setLength($length) {
		$this->length = $length;
	}

	public function setUseNumbers($useNumbers) {
		$this->useNumbers = $useNumbers;	
	}

	public function setUseSigns($useSigns) {
		$this->useSigns = $useSigns;	
	}
	
	public function setCaseSensitivity($caseSensitivity) {
		$this->caseSensitivity = $caseSensitivity;	
	}

	public function getPassword() {
		$this->generate();			
		return str_shuffle(htmlspecialchars($this->password)); 
	}

	private function generate() {
		for ($i=1; $i <= $this->length; $i++) {
			if($this->caseSensitivity) {
				if (rand(0, 1) == 1) {
					$this->password .= strtoupper($this->getLetter());
				} else {
					$this->password .= $this->getLetter();
				}
			} else {
				$this->password .= $this->getLetter();
			}
			if ($this->useNumbers) {
				$this->password .= $this->getNumber();
				$i++;				
			}
			if ($this->useSigns) {
				$this->password .= $this->getSign();
				$i++;				
			}			
		}
	}
	
	private function getLetter() {
		return $this->letters[rand(0,strlen($this->letters)-1)];
	}
	
	private function getNumber() {
		return rand(0, 9);
	}	
	
	private function getSign() {
		return ($this->signs[rand(0, strlen($this->signs)-1)]);
	}
	
}
?>