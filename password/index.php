<?php 
error_reporting(E_ALL | E_STRICT);
	
	if (isset($_REQUEST['submit'])) {
		require_once('password.php');	
		$password = new password;		
		$password->setLength($_REQUEST['length']);
		
		if (isset($_REQUEST['signs'])) {			
			$password->setUseSigns(TRUE);
		}
				
		if (isset($_REQUEST['numbers'])) {			
			$password->setUseNumbers(TRUE);
		}
		
		if (isset($_REQUEST['caseSensitivity'])) {			
			$password->setCaseSensitivity(TRUE);
		}	
	} else {
		$_REQUEST['length'] = 10;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<script type="text/javascript" src="js/fnc.js"></script>
		<title>Passwort Generator</title>
	</head>
	<body>
		<h1>..:: Passwort Generator ::..</h1>
		<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
			<table border="0">
				<tr>
					<td>L�nge:</td>				
					<td><input id="length" type="text" name="length" value="<?php echo $_REQUEST['length'] ; ?>" size="5"/></td>
					<td><input type="button" onclick="setLength('down');" value="-"></td>
					<td><input type="button" onclick="setLength('up');" value="+"></td>
				</tr>
				<tr>
					<td colspan="3">Sonderzeichen:</td>
					<td><input type="checkbox" name="signs" <?php echo (isset($_REQUEST['signs']) ? 'checked="checked"' : ""); ?>/></td>
				</tr>
				<tr>
					<td colspan="3">Zahlen:</td>
					<td><input type="checkbox" name="numbers" <?php echo (isset($_REQUEST['numbers']) ? 'checked="checked"' : ""); ?>/></td>
				</tr>
				<tr>
					<td colspan="3">Gro�-/Kleinschreibung:</td>
					<td><input type="checkbox" name="caseSensitivity" <?php echo (isset($_REQUEST['caseSensitivity']) ? 'checked="checked"' : ""); ?>/></td>
				</tr>
				<tr>
					<td colspan="4" align="center">
						<input type="submit" name="submit" value="Generieren" />
					</td>
				</tr>
			</table>
		</form>
		<?php if (isset($_REQUEST['submit'])) { ?>
			<h2><?php echo $password->getPassword(); ?></h2>
		<?php } ?>		
	</body>
</html>