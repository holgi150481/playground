#include <SPI.h>
#include <Ethernet.h>

// Ethernet Adressen
byte mac[] = {0x54, 0x55, 0x58, 0x10, 0x00, 0x24};
byte ip[]  = {192, 168, 0, 180};

#define ANALOG_SENSOR_1 A0
#define ANALOG_SENSOR_2 A1

EthernetServer server(80);

void setup() {
  Ethernet.begin(mac, ip);
  server.begin();
}

void loop() {
  EthernetClient client = server.available();
    if (client) {
      while (client.connected()) {
        if (client.available()) {
     
          // HTTP Header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/plain");
          client.println("Connnection: close");
          client.println("Access-Control-Allow-Origin: *"); 
          client.println();
           
          client.print("{");
          
          client.print("\"analog1\":");
          client.print("\"");
          client.print(analogRead(ANALOG_SENSOR_1));
          client.print("\",");
          
          client.print("\"analog2\":");
          client.print("\"");
          client.print(analogRead(ANALOG_SENSOR_2));
          client.print("\"");

          client.print("}");
          
          client.stop();
      }
    }
  }
