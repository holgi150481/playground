$(document).ready(function() {
	
	try {

		ws = new WebSocket("ws://192.168.0.180:80/");

		ws.onmessage = function(evt) {			
			var data = $.parseJSON(evt.data);			
	   		Gauge.Collection.get('sensor1').setValue(data.analog1);
	   		Gauge.Collection.get('sensor2').setValue(data.analog2);
		};

		ws.onerror = function(evt) {
			console.log(evt.data);
		};

		ws.onclose = function() {
			console.log("onclose called");
		};

		ws.onopen = function() {
			console.log("onopen called");
		};

	} catch(exception) {
		console.log(exception);
	}
	
});