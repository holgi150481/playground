#include <SPI.h>
#include <Ethernet.h>
#include <WebSocket.h>

#define ANALOG_SENSOR_1 A0
#define ANALOG_SENSOR_2 A1
#define LED 3

// Here we define a maximum framelength to 64 bytes. Default is 256.
#define MAX_FRAME_LENGTH 64

byte mac[] = { 0x52, 0x4F, 0x43, 0x4B, 0x45, 0x54 };
byte ip[] = { 192, 168, 0 , 180 };

WebSocket wsServer;

void onConnect(WebSocket &socket) {
  digitalWrite(LED, HIGH);
}

void onDisconnect(WebSocket &socket) {
  digitalWrite(LED, LOW);
}

void onData(WebSocket &socket, char* dataString, byte frameLength) {
  //socket.send(dataString, strlen(dataString));
}

void setup() {
  Ethernet.begin(mac, ip);
  
  pinMode(LED, OUTPUT);
  
  wsServer.registerConnectCallback(&onConnect);
  wsServer.registerDataCallback(&onData);
  wsServer.registerDisconnectCallback(&onDisconnect);  
  wsServer.begin();
  
  delay(100);
}

void loop() { 
  wsServer.listen();

  if (wsServer.isConnected()) {

    int valueOne = analogRead(ANALOG_SENSOR_1);
    int valueTwo = analogRead(ANALOG_SENSOR_2);
    
    String valueString;
    valueString = "{ \"analog1\":\"" + (String)valueOne + "\", \"analog2\":\"" + (String)valueTwo + "\" }";
     
    char message[50];
    valueString.toCharArray(message, 50);
    wsServer.send(message, strlen(message));
    Serial.println(message);
    
  }
  delay(100);
}