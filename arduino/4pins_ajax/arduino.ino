#include <SPI.h>
#include <Ethernet.h>

// Ethernet Adressen
byte mac[] = {0x54, 0x55, 0x58, 0x10, 0x00, 0x24};
byte ip[]  = {192, 168, 0, 180};

EthernetServer server(80);

#define RELAY_1  1
#define RELAY_2  2
#define RELAY_3  8
#define RELAY_4  9

String request; 

void setup(){
  Ethernet.begin(mac, ip);
  server.begin();

  pinMode(RELAY_1, OUTPUT);
  pinMode(RELAY_2, OUTPUT);
  pinMode(RELAY_3, OUTPUT);
  pinMode(RELAY_4, OUTPUT);
}

void loop(){
  
// ********************* HTTP Request auslesen *************
  EthernetClient client = server.available();
    if (client) {
      while (client.connected()) {
        if (client.available()) {
          
        char c = client.read();
        
        if (request.length() < 100) {
          request = request + c; 
        }
        
        if (c == '\n') { // Pruefen ob HTTP Request beendet ist dann HTML Ausgabe starten und Request auswerten. 
        
        // Request String nach Parameter durchsuchen (nicht gefunden ergibt int -1)
        if(request.indexOf("r1_on") > -1) {
          digitalWrite(RELAY_1, HIGH);
        }
        
        if(request.indexOf("r1_off") > -1) {
          digitalWrite(RELAY_1, LOW);
        }
              
        if(request.indexOf("r2on") > -1) {
          digitalWrite(RELAY_2, HIGH);
        }
        
        if(request.indexOf("r2off") > -1) {
          digitalWrite(RELAY_2, LOW);
        }
        
        if(request.indexOf("r3on") > -1) {
          digitalWrite(RELAY_3, HIGH);
        }
        
        if(request.indexOf("r3off") > -1) {
          digitalWrite(RELAY_3, LOW);
        }
        
        if(request.indexOf("r4on") > -1) {
          digitalWrite(RELAY_4, HIGH);
        }
        
        if(request.indexOf("r4off") > -1) {
          digitalWrite(RELAY_4, LOW);
        }
        
        if(request.indexOf("all_on") > -1) {
          digitalWrite(RELAY_1, HIGH);
          digitalWrite(RELAY_2, HIGH);
          digitalWrite(RELAY_3, HIGH);
          digitalWrite(RELAY_4, HIGH);
        }
        
        if(request.indexOf("all_off") > -1) {
          digitalWrite(RELAY_1, LOW);
          digitalWrite(RELAY_2, LOW);
          digitalWrite(RELAY_3, LOW);
          digitalWrite(RELAY_4, LOW);
        }


// ****************** HTML Ausgabe *********************
      
        // Pruefen welche Relais aktiv sind. 
        int r1 = digitalRead(RELAY_1);
        int r2 = digitalRead(RELAY_2);
        int r3 = digitalRead(RELAY_3);
        int r4 = digitalRead(RELAY_4);
      
        // HTTP Header Ausgeben
        client.println("HTTP/1.1 200 OK");
        client.println("Content-Type: text/html");
        client.println("Connnection: close");
        client.println("Access-Control-Allow-Origin: *"); 
        client.println();
        
        // HTML Header bei JSON Array nicht ausgeben!
        //client.println("<!doctype html>");
        //client.println("<html>");
        //client.println("<body>");
        
        client.print("{ ");
        
        client.print("\"r1\": ");
        if (r1 == 1) {
          client.print("\"on\", ");
        } else {
          client.print("\"off\", ");
        }
        
        client.print("\"r2\": ");
        if (r2 == 1) {
          client.print("\"on\", ");
        } else {
          client.print("\"off\", ");
        }
        
        client.print("\"r3\": ");
        if (r3 == 1) {
          client.print("\"on\", ");
        } else {
          client.print("\"off\", ");
        }
        
        client.print("\"r4\": ");
        if (r4 == 1) {
          client.print("\"on\"");
        } else {
          client.print("\"off\"");
        }
        
        client.print(" }");       
        
        //client.println("</body>");
        //client.println("</html>");  
        
        request = "";
        client.stop();
        }
      }
    }
  }
}