$(document).ready(function() {
	
	$('.button').click(function() {		
		$.ajax({
		   	url: "http://192.168.0.180",
		   	type: 'get',        	
		   	dataType: "json",
		   	data: {
		   		data: $(this).attr('data-pin') + "_" + $(this).attr('data-value')
		   	},
		   	success: function(response) {
		   		$('#a_status').css('background-color', response.r1 == "on" ? 'green' : 'red');
		   		$('#b_status').css('background-color', response.r2 == "on" ? 'green' : 'red');
		   		$('#c_status').css('background-color', response.r3 == "on" ? 'green' : 'red');
		   		$('#d_status').css('background-color', response.r4 == "on" ? 'green' : 'red');		   		
		   	}
		});
	});
	
});