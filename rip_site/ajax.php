<?php
require_once('functions.php');

define(FILES_FOLDER, '/var/www/playground/rip_site/files/');

if ($_REQUEST['art'] == 'load') {
	$fileList = loadFiles();
	echo $fileList;
}

if ($_REQUEST['art'] == 'delete') {
	$file = FILES_FOLDER . $_REQUEST['file'];
	if (file_exists($file)) {	
		shell_exec('rm -f ' . $file);
	}
	
	$fileList = loadFiles();
	echo $fileList;
}

if ($_REQUEST['art'] == 'add') {
	
	if ($_REQUEST['type'] =='png') {
		$tool = 'wkhtmltoimage';
	} else {
		$tool = 'wkhtmltopdf';
	}
	
	$filename = strtolower(parse_url($_REQUEST['url'], PHP_URL_HOST));
	$rip = 'xvfb-run ' . $tool . ' --use-xserver ' . $_REQUEST['url'] . ' ' . FILES_FOLDER . $filename . '.' . $_REQUEST['type'];
	shell_exec($rip);

	$fileList = loadFiles();
	echo $fileList;
}