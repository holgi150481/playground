$(document).ready(function() {
	loadData();

	$('#start_rip').click(function() {
		if (validateUrl($('#url').val())) {
			$('#ajax_loader').show();
			$.ajax({
	        	url: "ajax.php",
	        	type: 'post',        	
	        	dataType: "json",
	        	data: {
	        		art: 'add',
	        		url: $('#url').val(),
	        		type: $('#type').val(),
	        		greyscale: $('#greyscale').val(),
	        		lowquality: $('#lowquality').val(),        		
	        	},
	        	success: function(response) {
	        		$('#ajax_loader').hide();        		
	        		loadData();
	        	}
			});
		} else {
			alert ("URL fehlerhaft!");
		}
	});

});

function loadData() {
	$.ajax({
    	url: "ajax.php",
    	type: 'post',        	
    	dataType: "json",
    	data: {
    		art: 'load'
    	},
    	success: function(response) {
    		$("table").find("tr:gt(0)").remove();
    		if ($.isEmptyObject(response)) {    			
    			$('#table tr:last').after('<tr><td>Keine Daten...</td></tr>');
    		} else {
        		$.each(response, function(i, val) {        		
        			if (val.mime == 'application/pdf') {
        				var pic = '<img src="img/pdf.gif" width="16" height="16" title="PDF">&nbsp;&nbsp;';
        			} else if (val.mime == 'image/png') {
        				var pic = '<img src="img/pic.png" width="16" height="16" title="PNG">&nbsp;&nbsp;';
        			}
        			$('#table tr:last').after('<tr><td>' + pic + '<a href="files/' + val.name + '">' + val.name + '</a></td><td>' + val.size + '</td><td>' + val.date + '</td><td><img src="img/delete.png" class="delete_file" data-value="' + val.name + '"></td></tr>');
        		})
        		$('.delete_file').bind('click', function() {
        			deleteFile($(this).attr('data-value'));
    			});
    		}    		
    	}
	});
}

function deleteFile(file) {
	var a = confirm("Wirklch l�schen?");
	if (a) {
		$.ajax({
	    	url: "ajax.php",
	    	type: 'post',        	
	    	dataType: "json",
	    	data: {
	    		art: 'delete',      
	    		file: file
	    	},
	    	success: function(response) {	    		
	    		loadData();
	    	}
		});
	}
}

function validateUrl(value){
    return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             }