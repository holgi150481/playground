<?php

function format_bytes($a_bytes) {
	if ($a_bytes < 1024) {
		return $a_bytes .' Byte';
	} elseif ($a_bytes < 1048576) {
		return round($a_bytes / 1024, 2) .' KB';
	} elseif ($a_bytes < 1073741824) {
		return round($a_bytes / 1048576, 2) . ' MB';
	} elseif ($a_bytes < 1099511627776) {
		return round($a_bytes / 1073741824, 2) . ' GB';
	}
}

function loadFiles() {
	$folder = openDir(FILES_FOLDER);
	while ($file = readDir($folder)) {
		if ($file != "." && $file != ".." && $file != "index.html") {
			$data = array();
			$data['name'] = $file;
			$data['size'] = format_bytes(filesize(FILES_FOLDER . $file));
			$data['date'] = date("d.m.Y H:i", filemtime(FILES_FOLDER . $file));
			$data['mime'] = mime_content_type(FILES_FOLDER . $file);
			$files[] = $data;
		}
	}
	closeDir($folder);
	sort($files);

	return json_encode($files);
}