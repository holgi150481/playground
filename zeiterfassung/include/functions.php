<?php

function debug($var) {		
	$backtrace = debug_backtrace(FALSE);
	$output =  '<pre>';
	$output .=  'Aufruf: <strong>' . $backtrace[0]['file'] . '</strong> In Zeile: <strong>'. $backtrace[0]['line'] .  '</strong><br />';
    // TODO Variablen Namen korrekt auslesen. 
	foreach ($GLOBALS as $key => $value) {
		if($value === $var) {
			$varName = $key;
		} 
	}        
	$output .= 'Variable: <strong>' . (!empty($varName) ? '&#36;' . $varName : "Unbekannt")  . '</strong><br />';
	if (is_array($var) || is_object($var)) {
		$output .= print_r($var, TRUE);	
	} else {
	    ob_start();
        var_dump($var);
		$output .= ob_get_contents();
        ob_end_clean();
	}
	$output .= '</pre>';
    echo $output;

}

?>