<?php
//error_reporting(E_ALL | E_STRICT);
include('include/config.php');

if (isset($_REQUEST['start_stop'])) {
	$sql = 'SELECT id FROM ' . TABLE_TIME_RECORD . ' WHERE datum=CURDATE() AND ende IS NULL';
	$query = $db->query($sql);
	$entry = $query->fetch_assoc();
	
	if (empty($entry['id'])) {
		$sql = 'INSERT INTO ' . TABLE_TIME_RECORD . ' SET datum=CURDATE(), beginn=CURTIME()';
	} else {
		$sql = 'UPDATE ' . TABLE_TIME_RECORD . ' SET ende=CURTIME() WHERE id=' . $entry['id'];		
	}
	$db->query($sql);	
}

$sql = 'SELECT *, TIMEDIFF(ende , beginn) AS duration, TIMEDIFF(TIMEDIFF(ende , beginn), "'. WORKING_TIME . '") AS diff FROM ' . TABLE_TIME_RECORD;
$query = $db->query($sql);	

while ($row = $query->fetch_assoc()) {
	$data[] = $row;
}

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Zeiterfassung</title>
	</head>
	<body>
		<h1>..:: Zeiterfassung ::..</h1>
		
		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
			<input type="submit" name="start_stop" value="Start / Stop"/>
		</form>
		<br>
		<?php if (!empty($data)) { ?>
		<table cellpadding="0" cellspacing="0" width="50%" border="1">
			<tr>
				<th>Datum</th>
				<th>Beginn</th>
				<th>Ende</th>
				<th>Dauer</th>
				<th>Differenz</th>				
			</tr>	
			<?php foreach ($data as $value) { ?>
				<tr>
					<td><?php echo $value['datum']; ?></td>
					<td><?php echo $value['beginn']; ?></td>
					<td><?php echo $value['ende']; ?></td>
					<td><?php echo $value['duration']; ?></td>
					<td><?php echo $value['diff']; ?></td>					
				</tr>
			<?php } ?>
		</table>
		<?php } else { ?>
			<h2>Keine Daten vorhanden...</h2>
		<?php } ?>
		
	</body>
</html>