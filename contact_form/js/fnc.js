jQuery.extend(jQuery.validator.messages, {
	required: "Dieses Feld ist ein Pflichtfeld.",
	maxlength: jQuery.validator.format("Geben Sie bitte maximal {0} Zeichen ein."),
	minlength: jQuery.validator.format("Geben Sie bitte mindestens {0} Zeichen ein."),
	rangelength: jQuery.validator.format("Geben Sie bitte mindestens {0} und maximal {1} Zeichen ein."),
	email: "Geben Sie bitte eine g�ltige E-Mail Adresse ein.",
	url: "Geben Sie bitte eine g�ltige URL ein.",
	date: "Bitte geben Sie ein g�ltiges Datum ein.",
	number: "Geben Sie bitte eine Nummer ein.",
	digits: "Geben Sie bitte nur Ziffern ein.",
	equalTo: "Bitte denselben Wert wiederholen.",
	range: jQuery.validator.format("Geben Sie bitten einen Wert zwischen {0} und {1}."),
	max: jQuery.validator.format("Geben Sie bitte einen Wert kleiner oder gleich {0} ein."),
	min: jQuery.validator.format("Geben Sie bitte einen Wert gr��er oder gleich {0} ein."),
	creditcard: "Geben Sie bitte ein g�ltige Kreditkarten-Nummer ein.",
});

$(document).ready(function(){
	var validator = $('#form').validate({
		submitHandler: function() {
			validator.submit();
		},
		rules: {
			name: {
				required: true,
				maxlength: 100
			},
			mail: "email",
			homepage: "url",
			subject: { 
				required: true,
				maxlength: 100
			},
			message: {
				rangelength: [10,255],
				required: true
			}
		}
	})
	$('#reset').click(function() {
		validator.resetForm();
	})
});