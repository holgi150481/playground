<?php
if (!empty($_REQUEST['submit'])) {
	
	define('RECIPIENT_MAIL', 'dev@fafnir.ws');
	define('RECIPIENT_NAME', 'Holger Hartmann');
	
	if (empty($_REQUEST['name'])) {
		$info .= '<strong>Name</strong> ist ein Pflichtfeld!<br />';
	}
	if (empty($_REQUEST['mail'])) {
		$info .= '<strong>eMail</strong> ist ein Pflichtfeld!<br />';
	}
	if (empty($_REQUEST['subject'])) {
		$info .= '<strong>Betreff</strong> ist ein Pflichtfeld!<br />';
	}
	if (empty($_REQUEST['message'])) {
		$info .= '<strong>Nachricht</strong> ist ein Pflichtfeld!<br />';
	}	
	
	if (empty($info)) {

		$message  = '<pre>' . $_REQUEST['message'];
		if (!empty($_REQUEST['homepage'])) {
			$message .= '<br />Homepage: ' . $_REQUEST['homepage'];
		}		
		$message .= '</pre>';
		
		$header  = 'MIME-Version: 1.0' . "\r\n";
		$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$header .= 'To: ' . RECIPIENT_NAME . ' <' . RECIPIENT_MAIL . '>' . "\r\n";
		$header .= 'From: ' . $_REQUEST['name'] . ' <' . $_REQUEST['mail'] . '>' . "\r\n";		
			
		mail(RECIPIENT_MAIL, $_REQUEST['subject'], $message, $header);
		
		$info = "eMail wurde korrekt versendet.";
		$class = "mailOk";
		$_REQUEST = array();		
	} else {
		$class = "mailError";
	}
	
} 
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="ISO-8859-1">
        <link type="text/css" rel="stylesheet" href="css/style.css" />
        <script type="text/javascript" src="js/jquery-1.8.2.js"></script>
        <script type="text/javascript" src="js/jquery.validate.js"></script>
        <script type="text/javascript" src="js/fnc.js"></script>
        <title>Contact Form</title>
    </head>
    <body>
    	<h1>..:: Contact Form ::..</h1>
        <?php if (!empty($info)) { ?>
        	<div class="message <?php echo $class ?>"><?php echo $info; ?></div>
        <?php } ?>
        <label class="error"></label>
        <form id="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <fieldset>
                <legend>eMail Senden</legend>
                <div>
                    <label for="name" class="required">Name:</label>
                    <input id="name" name="name" type="text" value="<?php echo $_REQUEST['name'] ?>"/>
                </div>
                <div>
                    <label for="mail" class="required">eMail:</label>
                    <input name="mail" type="text" value="<?php echo $_REQUEST['mail'] ?>" />
                </div>
                <div>
                    <label for="homepage">Homepage:</label>
                    <input name="homepage" type="text" value="<?php echo $_REQUEST['homepage'] ?>"/>
                </div>
                <div>
                    <label for="subject" class="required">Betreff:</label>
                    <input name="subject" type="text" value="<?php echo $_REQUEST['subject'] ?>"/>
                </div>
                <div>
                    <label for="message" class="required">Nachricht:</label>
                    <textarea name="message"><?php echo $_REQUEST['message'] ?></textarea>
                </div>
                <div>
                    <input type="submit" name="submit" value="Absenden"/>
                    <input type="reset" id="reset" value="L&ouml;schen"/>
                </div>
            </fieldset>
        </form>         
    </body>
</html>