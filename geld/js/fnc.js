var controlValue = 0;

// Berechnung �ber Buttons
function countMoney(currency, direction, art, value) {
	if (direction == 'down') {				
		quantity = document.getElementById('text' + currency).value;
		// Pr�fen ob Negativ Werte vorliegen
		if (quantity < 1) {
			alert ("Achtung: Anzahl darf nicht negativ sein!");	
			return null;		
		};		
		quantity--;
		document.getElementById('text' + currency).value = quantity;
		amount = quantity * value;
		document.getElementById('currency' + currency).innerHTML = amount.toFixed(2) + ' &euro;';
	} else {		
		quantity = document.getElementById('text' + currency).value;
		quantity++;
		document.getElementById('text' + currency).value = quantity;
		amount = quantity * value;
		document.getElementById('currency' + currency).innerHTML = amount.toFixed(2) + ' &euro;';
	}
	report();	
}

// Berechnung aus Textbox
function countMoneyfromText(textbox, currency, value) {	
	if (textbox.value != '' &&!isNaN(textbox.value)) {
		amount = textbox.value * value;
		document.getElementById('currency' + currency).innerHTML = amount.toFixed(2) + ' &euro;';	
	} else {
		textbox.value = '0';
		document.getElementById('currency' + currency).innerHTML = '0.00 &euro;';
	};
	report();
}

function report() {
	// M�nzen ausz�hlen. 
	var amountCoins = 0;
	var countCoins = 0;
	var coinTotal = document.getElementsByName('coinTotal');
	var coinCount = document.getElementsByName('coinCount');		
	for (var i=0; i <= 7; i++) {				
		amountCoins = amountCoins + parseFloat(coinTotal[i].innerHTML);
		countCoins = countCoins + parseInt(coinCount[i].value);				
	};		
	document.getElementById('amountCoins').innerHTML = amountCoins.toFixed(2) + ' &euro;';
	document.getElementById('countCoins').innerHTML = countCoins;
	
	// Scheine Ausz�hlen
	var amountNotes = 0;
	var countNotes = 0;
	var noteTotal = document.getElementsByName('noteTotal');
	var noteCount = document.getElementsByName('noteCount');		
	for (var i=0; i <= 6; i++) {				
		amountNotes = amountNotes + parseFloat(noteTotal[i].innerHTML);
		countNotes = countNotes + parseInt(noteCount[i].value);				
	};		
	document.getElementById('amountNotes').innerHTML = amountNotes.toFixed(2) + ' &euro;';
	document.getElementById('countNotes').innerHTML = countNotes;
	
	// Gesamt Eintr�ge ausgeben
	document.getElementById('countAll').innerHTML = countNotes + countCoins;
	document.getElementById('amountAll').innerHTML = (amountCoins + amountNotes).toFixed(2) + ' &euro;';
	
	checkControlValue(amountCoins + amountNotes);	
}

// Differenz setzen
function setControlValue(value) {
	if (value.value != '' && !isNaN(value.value)) {
		controlValue = parseFloat(value.value);		
		value.value = controlValue.toFixed(2);	
	} else {
		controlValue = 0;
		value.value = "0.00 �";
	}
	console.log(controlValue);
	document.getElementById('controlValue').innerHTML = controlValue.toFixed(2) + ' &euro;';	
}

// Differenz berechnen
function checkControlValue(value) {
	if (controlValue > 0) {		
		balance = value - controlValue;		
		document.getElementById('controlValue').innerHTML = balance.toFixed(2) + ' &euro;';
	} 	
}
