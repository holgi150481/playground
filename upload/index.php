<?php
error_reporting(E_ALL | E_STRICT);

function format_bytes($a_bytes)
{
	if ($a_bytes < 1024) {
		return $a_bytes .' Byte';
	} elseif ($a_bytes < 1048576) {
		return round($a_bytes / 1024, 2) .' KB';
	} elseif ($a_bytes < 1073741824) {
		return round($a_bytes / 1048576, 2) . ' MB';
	} elseif ($a_bytes < 1099511627776) {
		return round($a_bytes / 1073741824, 2) . ' GB';
	}
}

if (isset($_FILES['datei'])) {
	move_uploaded_file($_FILES['datei']['tmp_name'], 'upload/' . $_FILES['datei']['name']);
}
?>

<!doctype html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Upload</title>		
	</head>
	<body>
		<h1>..:: Upload ::..</h1>
		<form method="post" enctype="multipart/form-data">
			<input type="file" name="datei" />
    		<input type="submit" value="Upload!" />
    	</form>
    	<hr />
    	<table cellpadding="2">
    		<?php if (count(scandir('upload')) > 2) { ?>
    			<tr>
    				<th>Datum</th>
    				<th>Dateiname</th>
    				<th>Gr&ouml;&szlig;e</th>
    			</tr>    		
    			<?php
    			$verz = openDir('upload');    			
				while ($file = readDir($verz)) {	 
 					if ($file != "." && $file != "..") { ?>	
						<tr>
							<td><?php echo date('d.m.Y H:i', filemtime('upload/' . $file)); ?></td>						
							<td><a href="upload/<?php echo $file; ?>"><?php echo $file; ?></a></td>
							<td><?php echo format_bytes(filesize('upload/' . $file)); ?></td>						
						</tr>
						<?php 
					}
				}
				closeDir($verz);
			} else {
			    ?>
			    <tr>
			        <th>Keine Daten vorhanden. Lade etwas hoch...</th>
			    </tr>
			    <?php
			}
			?>
		</table>		
	</body>
</html>